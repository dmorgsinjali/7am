import java.util.*;

public class Test
{
	public static void main(String args[])
	{

		System.input = new Scanner(System.in);

		try
		{
			System.out.println("Enter value for var1: ");
			int var1 = input.nextInt();
			System.out.println("Enter value for var2: ");
			int var2 = input.nextInt();

			int product = var1 * var2;
			System.out.println("the product is: " + product);
		}

		catch(InputMismetchException ex)
		{
			System.out.println("Your have entered a non-whole number");
		}
	}
}