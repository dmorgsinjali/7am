import  java.util.Scanner;

public class Control
{
	public static void main(String args[] )
	{
		Scanner input = new Scanner(System.in);
		System.out.print("Please enter your age: \n");
		int myAge= input.nextInt();

		if(myAge == 25)
		{
			System.out.println("Your are 25 years old");
		}
		else
		{
			System.out.println("We don't know your age; however, you're not 25");
		}
	}
}