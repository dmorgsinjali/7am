public abstract class Salary{
	abstract void salary();
	abstract void bonus();

	public static void main(String[] args){
		AbstractDemo salary= new AbstractDemo(){
			@Override 
			public void add (){
				System.out.println("hallo");
			}
		};
		AbstractDemo bonus= new AbstractDemo(){
			@Override 
			public void add (){
				System.out.println("Hi");
			}
		};

		salary.add();
		bonus.add();
	}
}