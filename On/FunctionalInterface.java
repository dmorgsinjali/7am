interface Calculator{

	int sum(int a, int b);

	// -> == lambda expriatin 
	// lambda expration can be only used on functional interface


}
public class FunctionalInterface{
	public static void main(String[] args){
		Calculator calculator= new Calculator(){
			@Override
			public int sum(int a, int b){
				return a+b;
			}
		};
		System.out.println(calculator.sum(4,5));
 	}
}