import java.util.Scanner;

public class Switch
{
	public static void main(String args[] )
	{
		Scanner input = new Scanner(System.in);
		int tenD;
		System.out.print("Please enter your fav number"); 
		tenD = input.nextInt();

		switch (tenD)
		{
			case 1:
				System.out.println("you entered 1");
				break;
			case 5:
				System.out.println("You entered five");
				break;
			default:
				System.out.println("You did net enter either 1 or five");
				break;
		}
	}
}