import java.util.Scanner;

public class Even
{
	public static void main(String args[] )
	{
		Scanner input = new Scanner(System.in);

		int sum = 0;

		System.out.print("Please enter a whole integer , or enter '0' to exit the loop: ");

		int myNamber = input.nextInt();

		while(myNamber != 0)
		{
			sum += myNamber;
			System.out.print("Please enter a whole integer , or enter '0' to exit the loop: ");
			myNamber = input.nextInt();
		}

		System.out.println("Your sum was: " + sum);
	}
}