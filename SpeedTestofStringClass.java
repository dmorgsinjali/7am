public class SpeedTestofStringClass{
	public static void main(String[] args){
	
	long timel=System.currentTimeMillis();
	StringBuilder sub= new StringBuilder("qwrtyuiopasdfthlzxcubmn");
	
	for (int i = 0; i<1000000;i++){
	sub.append("1234567890=qwertyuioup[]asdfghjkl;zxcvbnu,./");
	}
	System.out.println("Time Taken by StringBuilder"+(System.currentTimeMillis()-timel)+ "ms");
	
	long time2=System.currentTimeMillis();
	 StringBuffer sdf = new StringBuffer("qwrtyuiopasdfthlzxcubmn");
	 
	for (int i = 0; i<1000000;i++){
	sdf.append("1234567890=qwertyuioup[]asdfghjkl;zxcvbnu,./");
	}
	System.out.println("Time Taken by StringBuilder"+(System.currentTimeMillis()-time2)+"ms");
	
	
	}
}