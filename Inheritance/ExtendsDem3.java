class ParentClass{
	public long Father(){
		return 1;
	}
	public String Hair(){
		return "Black";

	}
}
public class ExtendsDemo3 extends ParentClass

{
	@Override
	public  long Father(){
		return 4;
	}
	
	public static void main(String[] args){
		ExtendsDemo3 ed= new ExtendsDemo3();
		System.out.println(ed.Father());
		System.out.println(ed.Hair());
	}
}