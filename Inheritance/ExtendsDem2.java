class ParentClass{
	public long add(long a, long b){
		return a+b;
	}
}

class SecondParent extends ParentClass{
	public long square(long a){
		return a*a;
	}
}

public class ExtendsDemo2 extends SecondParent


{

	@Override
	public long add(long a, long b){
		return a*b;
	}
	public static void main(String[] args){
		ExtendsDemo2 ed=new ExtendsDemo2();
		System.out.println(ed.add(45,66));
		System.out.println(ed.square(4));

	}

}