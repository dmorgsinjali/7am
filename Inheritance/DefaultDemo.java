//2. Implemennts:
//-> when you are inheriting from parent that is interface



import java.util.Date;


interface DefaultType{
  void hi();
  default Date getLatestDete(){
  	return new Date();
  }
}

public class DefaultDemo implements DefaultType
{

	@Override
	public void hi(){
		System.out.println("hello, i am hi");
	}

	public static void main(String[] args){
		DefaultDemo dd= new DefaultDemo();
		dd.hi();
		System.out.println(dd.getLatestDete());
	}
}