class ParentClass{
	public long add(long a, long b){
		return a+b;
	}
}

class SecondParent{
	public long square(long a){
		return a*a;
	}
}

public class ExtendsDemo extends ParentClass
{
	public static void main(String[] args){
		ExtendsDemo ed=new ExtendsDemo();
		System.out.println(ed.add(45,66));
	}

}