interface Father{
	String getProperty();

}
interface Mother{
	String getEyeColor();
}
public class Son implements Father,Mother{
	@Override
	public String getProperty(){
	return "home";
	}
	@Override
	public String getEyeColor(){
	return "brown";
	}
	public static void main(String[] agrs){
	Son s=new Son();
	System.out.println(s.getProperty());
	System.out.println(s.getEyeColor());
	}
}