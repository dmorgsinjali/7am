--> OOP Property of OOP
--> Two ways of implementing inheritance in java:
a. By using extends keyword.
b. By using implements keyword.

Main use of inheritance: Reusablity of codes.

//Download.javan  --> parent

//project1download inheritance parent

1. extends:
--> When you are inheritance properties from class as a parent
--> multiple inheritance is not supported in java class inheritance.
--> since our class already extends object class there is a kind of multiple
	inheritance when you extends a parent class.