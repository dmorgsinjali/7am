public class MathodOverloading1{

	MathodOverloading1(){
		System.out.println("hello i am blank constructor");
	}
	MathodOverloading1(String name){
		System.out.println("hello "+ "i am parametrized constructor");
	}
	public int add(int a, int b){
		return a+b;

	}
	public int add(int a, int b);
	public float add(float a, float b){
		return a+b;

	}
	public float add(float a, float b){
		return a+b;

	}


	public static void main(String[] args){
		MathodOverloading1 mo = new MathodOverloading1();
		System.out.println(mo.add(2,3));
		System.out.println(mo.add(4.5f,3.4f));
		System.out.println(mo.add(4.5f,3.4f));
	}
}