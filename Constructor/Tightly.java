Tightly encapsulated class:
-> a class is said to be tightly encapsulated 
	if all the variables declared are private 
-> whether or not the mathods are declared private or punlic

eg:
	public class Hi(){
		private int id; // tightly encpasulated class
	}
	public class Hello(){// tightly encpasulated class
		private int id;
		private void setHi(){
			sout("hi");
		}

	}
	class hi{// this class is not tightly encapsulated
		int id;
		private void setSomething(){
			sout("hello");
		}
	}

					
Abstraction:
-> hiding the real detail of the work with the help of interface.
-> We use interface as the service to hide the real details of our longic

eg:
	interface Driving{
		int wheel=4;
		int steering=1;

		void drive();
		void horn();
	} 
	class Car implements Driving{
		@Override
		public void drive(){
			// real logi is here
		}
	}