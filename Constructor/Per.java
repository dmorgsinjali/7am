class Per{
	private int id;
	private String name;
	Per(){}

	public int getId(){
		return id;

	}

	public void setId(int id){
		this.id=id;
	}
	public String getName(){
		return name;
	}
	public void setName(String name){
		this.name=name;
	}
}

public class perEx{

	public static void main(String[] args){
		Per p= new Per(); // 12 bit
		p.setId(1);// 32 bit
		p.setName("fan");//255 byte


		System.out.println(p.getId());
		System.out.println(p.getName());
	}
}